describe('/register', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('has a client type', () => {
    cy.get('[data-cy=registration-cards] :nth-child(2) > .q-card-media')
      .should('contain', 'Client')
  })

  it('creates a client type registration', () => {
    cy.get('[data-cy=registration-cards] :nth-child(2) > .q-card-media').click().end()
    cy.get('.modal-content').contains('h4', 'Client Account Creation')
  })

  it('creates a client type registration', () => {
    cy.get('[data-cy=registration-cards] :nth-child(2) > .q-card-media').click().end()
    cy.get('.modal-content').contains('h4', 'Client Account Creation')
  })

  it('requires email', () => {
    cy.get('[data-cy=registration-cards] :nth-child(2) > .q-card-media').click().end()
    cy.get('[data-cy=user-form-registration]').contains('Connect').click()
    cy.get('[data-cy=email] > .col > div')
      .should('have.css', 'color', 'rgb(219, 40, 40)')
  })

  context('user registration form functionality', () => {
    beforeEach(() => {
      cy.get(':nth-child(2) > .q-card-media').click().end()
      cy.get('[data-cy=email] .q-input-target').type('test@kpapro.com')
    })

    it('requires password', () => {
      cy.get('[data-cy=user-form-registration]').contains('Connect').click()
      cy.get('[data-cy=password] > .col > div')
        .should('have.css', 'color', 'rgb(219, 40, 40)')
      cy.get('.q-notification-list > .q-notification .q-alert-content')
        .should('contain', 'Please review fields again.')
    })

    it('verifies password', () => {
      cy.get('[data-cy=password] .q-input-target').type('newpassword')
      cy.get('[data-cy=password-match] .q-input-target').type('newpassword!')
      cy.get('[data-cy=user-form-registration]').contains('Connect').click()
      cy.get('[data-cy=password-match] > .col > div')
        .should('have.css', 'color', 'rgb(219, 40, 40)')
      cy.get('.q-notification-list > .q-notification .q-alert-content')
        .should('contain', 'It doesn\'t look like your passwords match!')
    })
  })
})
