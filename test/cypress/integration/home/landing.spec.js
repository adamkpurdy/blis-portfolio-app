
describe('Landing', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('Has a Logo', () => {
    cy.get('.logo')
      .should('be.visible')
  })

  it('The greeting', () => {
    cy.get('h6.desc')
      .contains('Which type of account would you like to')
    cy.get('h6.desc')
      .contains('log into?')
  })

  it('The page has a registration link', () => {
    cy.get('[data-cy=registration-link]')
      .contains('register')
  })

  it('The page has a "Creator" account type', () => {
    cy.get('[data-cy=creators]')
      .contains('Creator')
  })

  it('The page has a "Client" account type', () => {
    cy.get('[data-cy=clients]')
      .contains('Client')
  })
})
