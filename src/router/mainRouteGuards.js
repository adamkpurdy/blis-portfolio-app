export default (to, from, next, $fb) => {
  const currentUser = $fb.auth().currentUser
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const requiresReCaptcha = to.matched.some(record => record.meta.requiresReCaptcha)
  const confirmationResult = to.params.confirmationResult

  if (requiresAuth && !currentUser) next('/')
  else if (requiresReCaptcha && !confirmationResult) next('/')
  else return false
}
