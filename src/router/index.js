import firebase from 'firebase/app'
import Vue from 'vue'
import VueRouter from 'vue-router'

import adminRoutes from './adminRoutes.js'
import creatorRoutes from './creatorRoutes.js'
import mainRoutes from './mainRoutes.js'
import registrationRoutes from './registrationRoutes.js'
import authRoutes from './authRoutes.js'

import mainRouteGuards from './mainRouteGuards.js'

const routes = [
  ...adminRoutes,
  ...authRoutes,
  ...creatorRoutes,
  ...mainRoutes,
  ...registrationRoutes
]

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach((to, from, next) => {
    firebase.auth().onAuthStateChanged(() => {
      if (mainRouteGuards(to, from, next, firebase)) return
      next()
    })
  })

  return Router
}
