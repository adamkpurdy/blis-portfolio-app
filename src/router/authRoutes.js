export default [
  {
    name: 'AdminLogin',
    path: '/auth/admin',
    component: () => import('pages/auth/Login.vue')
  },
  {
    name: 'ClientLogin',
    path: '/auth/client',
    component: () => import('pages/auth/Login.vue')
  },
  {
    path: '/auth',
    component: () => import('layouts/Basic.vue'),
    children: [
      {
        name: 'ForgotPasword',
        path: 'forgotPassword'
        // component: () => import('components/user/dashboardProfile.vue')
      }
    ]
  },
  {
    name: '',
    path: '/auth',
    component: () => import('layouts/Main.vue'),
    children: [
      {
        name: 'CreatorLogin',
        path: 'creator',
        component: () => import('pages/auth/PhoneNumber.vue')
      }
    ]
  }
]
