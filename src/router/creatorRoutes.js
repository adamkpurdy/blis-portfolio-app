export default [
  {
    path: '/user/creator/',
    component: () => import('layouts/User.vue'),
    props: true,
    meta: {
      requiresAuth: true
    },
    children: [
      {
        name: 'Home',
        path: '',
        component: () => import('pages/user/dashboard/creator/Home.vue')
      },
      {
        name: 'UserJobs',
        path: 'jobs',
        component: () => import('pages/user/dashboard/creator/Jobs.vue')
      },
      {
        name: 'Messages',
        path: 'messages',
        component: () => import('components/user/Messages.vue')
      },
      {
        name: 'UserProfile',
        path: 'profile',
        component: () => import('pages/user/dashboard/creator/Profile.vue')
      }
    ]
  }
  // {
  //   path: '/public/user',
  //   component: () => import('layouts/Public.vue'),
  //   children: [
  //     {
  //       name: 'PublicProfile',
  //       path: ':name',
  //       component: () => import('components/user/profile/Index.vue')
  //     }
  //   ]
  // }
]
