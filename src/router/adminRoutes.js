export default [
  {
    path: '/user/admin',
    component: () => import('layouts/Admin.vue'),
    children: [
      {
        path: '',
        name: 'Overview',
        component: () => import('pages/user/dashboard/admin/AdminOverView.vue')
      },
      {
        path: 'companies',
        name: 'Companies',
        component: () => import('components/admin/Companies.vue')
      },
      {
        path: 'creators',
        name: 'Creators',
        component: () => import('components/admin/Creators.vue')
      },
      {
        path: 'applications',
        name: 'Applications',
        component: () => import('components/admin/Applications.vue')
      },
      {
        path: 'jobs',
        name: 'OpenJobs',
        component: () => import('components/admin/Jobs.vue')
      }
    ]
  }
]
