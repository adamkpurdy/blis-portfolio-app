export default [
  {
    name: 'Registration',
    path: '/registration',
    component: () => import('pages/Index.vue')
  },
  {
    name: '',
    path: '/registration',
    component: () => import('layouts/Main.vue'),
    children: [
      {
        name: 'CreatorRegistration',
        path: 'creatorInfo',
        component: () => import('pages/auth/CreatorWebOnboarding.vue')
      },
      {
        name: 'PhoneNumber',
        path: 'creatorPhoneNumber',
        component: () => import('pages/auth/PhoneNumber.vue'),
        props: true
      },
      { // TODO: Add some type of route guard for the captcha
        // object be present so route isn't arbitrarily
        // available publicly
        name: 'Verification',
        path: 'verification',
        props: true,
        component: () => import('pages/auth/Verification.vue'),
        meta: {
          requiresReCaptcha: true
        }
      }
    ]
  },
  // {
  //   name: 'Onboarding',
  //   path: '/registration/creator/onboarding',
  //   props: true,
  //   component: () => import('pages/auth/Onboarding.vue')
  // },
  {
    name: 'ClientRegistration',
    path: '/registration/client',
    component: () => import('pages/auth/CompanyRegistration.vue')
  }

]
