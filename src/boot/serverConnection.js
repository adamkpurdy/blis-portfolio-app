import * as firebaseService from '../services/FirebaseService'
import { firestorePlugin } from 'vuefire'

export default ({ app, router, Vue }) => {
  firebaseService.fBInit(process.env.BlisConfig.FIREBASE_CONFIG)
  Vue.use(firestorePlugin)
  Vue.prototype.$fb = firebaseService
  Vue.prototype.$db = firebaseService.firestore()
}
