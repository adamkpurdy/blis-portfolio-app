import axios from 'axios'

export default async ({ Vue }) => {
  // axios.baseURL = endpointUrl
  Vue.prototype.$axios = axios
}
