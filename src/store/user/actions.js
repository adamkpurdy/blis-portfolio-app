export const saveOnboardingData = ({ commit }, data) => {
  commit('UPDATE_USER_DATA', data)
}

export const updateUserData = ({ commit }, data) => {
  commit('UPDATE_USER_DATA', data)
}
