export const setEditDialog = ({ commit }, data) => {
  commit('SET_EDIT_DIALOG', data)
}

export const setUserFormType = ({ commit }, data) => {
  commit('SET_USER_FORM_TYPE', data)
}

export const setUserProfileData = ({ commit }, data) => {
  commit('SET_USER_PROFILE_DATA', data)
}

export const setUserVideos = ({ commit }, data) => {
  commit('SET_USER_VIDEOS', data)
}

export const setUserPastProjects = ({ commit }, data) => {
  commit('SET_USER_PAST_PROJECTS', data)
}

export const updateUserProfileData = ({ commit }, data) => {
  commit('UPDATE_USER_PROFILE_DATA', data)
}
