export const SET_EDIT_DIALOG = (state, data) => {
  state.editDialog = data
}

export const SET_USER_FORM_TYPE = (state, data) => {
  state.formType = data
}

export const SET_USER_PROFILE_DATA = (state, data) => {
  debugger
  state.currentUser = data
}

export const SET_USER_VIDEOS = (state, data) => {
  state.videos = data
}

export const SET_USER_PAST_PROJECTS = (state, data) => {
  state.pastProjects = data
}

export const UPDATE_USER_PROFILE_DATA = (state, data) => {
  if (data.value) {
    state.currentUser[data.prop] = data.value
  } else {
    state.currentUser = Object.assign({}, state.currentUser, data)
  }
}
