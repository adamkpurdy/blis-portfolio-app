import profile from './profile'
import onboarding from './onboarding'
import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'
import state from './state'

const user = {
  getters,
  mutations,
  actions,
  state,
  modules: {
    onboarding,
    profile
  }
}
export default user
