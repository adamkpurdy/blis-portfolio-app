/** UPDATE_USER_DATA - writeStateData Mutation
 * @param  {Object} state - state module
 * @param  {Object} data.prop - name of the state data
 * @param  {Object} data.value - value of state data
 */
export const UPDATE_USER_DATA = (state, data) => {
  state[data.prop] = data.value
}
