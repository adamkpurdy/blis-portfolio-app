
import * as firebaseService from '../../../services/FirebaseService.js'
import { tokenizeFullName } from '../../../utils/utilFunctions.js'
import User from '../../../models/User.js'

/** verifyUserAndAuthenticate
 * @param  {Function} {commit - commit function
 * @param  {Object} {state} - module state
 * @param  {Object} data.confirmConfirmationResult - recaptch object
 * @param  {Object} data.verificationCode - verfication supplied from firebase
 *
 * @return {Promise}
 */
export const verifyUserAndAuthenticate = ({ state }, data) => {
  const { confirmationResult, verificationCode } = data
  return new Promise(async (resolve, reject) => {
    let id = ''
    let authUser = {}
    let currentUser = {}
    let isOnboarded = false
    let userUpdateValues = []

    try {
      // CHECK FOR A VALID AUTH CODE &&
      // GET AUTHENTICATED USER FROM FIREBASE
      authUser = await confirmationResult.confirm(verificationCode)

      // CHECK IF USER EXISTS AND HAS ONBOARDED
      id = authUser.user.uid
      isOnboarded = await getOnboarding(id, firebaseService)

      // HANDLE THE ONBOARDED STATE
      // CHECK IF USER HAS ONBOARDED
      if (isOnboarded.exists && isOnboarded.data().onboarded) {
        resolve()
      } else {
        userUpdateValues = await Promise.all([
          updateUsersCollection(id, state, firebaseService),
          updateAuthEmail(state, authUser)
        ])

        currentUser = userUpdateValues[0]
        resolve({
          prop: 'currentUser',
          value: currentUser
        })
      }
    } catch (err) {
      reject(`[VERIFY CODE AND AUTHENTICATE ERROR]: ${err}`)
    }
  })
}

/** getOnboardingComplete - function to check if the user
 * has already been onboarded against the user's collection
 *
 * @param  {String} id
 *
 * @return {Promise}
 */
export const getOnboarding = async (id, firebaseService) => {
  // abstract all collections into constants file
  const userRef = firebaseService.firestore().collection('users').doc(id)
  return userRef.get()
}

/** updateAuthEmail - function to update user's email to
 * firebase's auth db
 *
 * @param  {Object} state
 *
 * @returns {Promise}
 */
export const updateAuthEmail = async (state, authUser) => {
  return authUser.user.updateEmail(state.email)
}

/** updateUsersCollection - function to write user to users
 * collection in firestore
 *
 * @param  {Object} state - user.onboarding
 * @param  {String} id - id of the authentication user against
 * firebase phone authentication
 *
 * @return {Promise}
 */
export const updateUsersCollection = (id, state, firebaseService) => {
  return new Promise(async (resolve, reject) => {
    const user = new User(setupUserDoc(state, firebaseService))
    const userRef = firebaseService.firestore().collection('users').doc(id)
    userRef.set(user)
      .then(() => {
        resolve(user)
      })
      .catch(err => {
        console.error(`[updateUsersCollectin] Error: ${err}`)
        reject(err)
      })
  })
}
/** setupUserDoc - function to create a user model
 *
 * @param  {Object} state
 *
 * @return {Object} userDoc
 */
export const setupUserDoc = (state, firebaseService) => {
  const fbRolesMap = {}
  const location = {
    city: state.location,
    geo: firebaseService.getGeoPoint(34.053345, -118.242349),
    subtitle: '',
    title: ''
  }
  state.roles.forEach((v) => {
    fbRolesMap[v.id] = v
  })

  const userDoc = {
    ...tokenizeFullName(state.fullName),
    biography: '',
    email: state.email,
    links: [],
    location: location,
    roleRefs: state.roles.map(r => (`/roles/${r.id}`)),
    roles: fbRolesMap,
    onboarded: true,
    phoneNumber: state.phoneNumber,
    profilePhoto: '',
    verificationStatus: ''
  }
  return userDoc
}
