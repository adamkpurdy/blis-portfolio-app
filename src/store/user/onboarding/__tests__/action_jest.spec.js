import {
  getOnboarding,
  setupUserDoc,
  updateAuthEmail,
  updateUsersCollection,
  verifyUserAndAuthenticate
} from '../actions.js'

const id = 'someID'
const state = {
  email: 'jc@doe.com',
  fullName: 'John C. Doe',
  location: 'Los Angeles',
  phoneNumber: '+1 (800) 588-2300',
  roles: []
}

const data = {
  confirmationResult: {
    confirm: jest.fn((verficationCode) => {
      return {
        user: {
          uid: 'someId'
        }
      }
    })
  },
  verficationCode: 'someVerificationCode'
}

const firebaseServiceMockReject = {
  getGeoPoint: jest.fn((lat, lon) => {
    return [lat, lon]
  }),
  firestore: jest.fn(() => {
    return {
      collection: jest.fn(() => {
        return {
          doc: jest.fn(() => {
            return {
              set: jest.fn(() => {
                return Promise.reject()
              })
            }
          })
        }
      })
    }
  })
}

const firebaseServiceMock = {
  getGeoPoint: jest.fn((lat, lon) => {
    return [lat, lon]
  }),
  firestore: jest.fn(() => {
    return {
      collection: jest.fn(() => {
        return {
          doc: jest.fn((id) => {
            return {
              get: jest.fn(() => {
                return {
                  id: id
                }
              }),
              set: jest.fn(() => {
                return Promise.resolve()
              })
            }
          })
        }
      })
    }
  })
}

describe('#verfiyUserAndAuthenticate', () => {
  test('Returns a current user object', () => {
    const confirm = jest.spyOn(data.confirmationResult, 'confirm')
    verifyUserAndAuthenticate({ state }, data)
    expect(confirm).toHaveBeenCalled()
  })
})

describe('Onboarding Actions: ', () => {
  describe('#getOnboarding', () => {
    test('Return a user when called', () => {
      return expect(getOnboarding(id, firebaseServiceMock)).resolves.toHaveProperty('id', 'someID')
    })
  })

  describe('#udpateAuthEamil', () => {
    test('Returns true when resolves', () => {
      const authUser = {
        user: {
          updateEmail: jest.fn(() => {
            return Promise.resolve()
          })
        }
      }
      return expect(updateAuthEmail(state, authUser)).resolves.toBe()
    })
    test('Returns false when rejected', () => {
      const authUser = {
        user: {
          updateEmail: jest.fn(() => {
            return Promise.reject()
          })
        }
      }
      return expect(updateAuthEmail(state, authUser)).rejects.toBe()
    })
  })

  describe('#setupUserDoc', () => {
    test('Returns a user object when supply with a state object', () => {
      const userDoc = setupUserDoc(state, firebaseServiceMock)
      expect(userDoc).toHaveProperty('firstName', 'John')
      expect(userDoc).toHaveProperty('middleName', 'C.')
      expect(userDoc).toHaveProperty('lastName', 'Doe')
      expect(userDoc).toHaveProperty('location', {
        city: 'Los Angeles',
        geo: [34.053345, -118.242349],
        subtitle: '',
        title: ''
      })
    })
  })

  describe('#updateUsersCollection', () => {
    test('it should resolve with a user object', () => {
      return expect(updateUsersCollection(id, state, firebaseServiceMock)).resolves.toHaveProperty('isAdmin', false)
    })

    test('it should reject with an error user object', () => {
      return expect(updateUsersCollection(id, state, firebaseServiceMockReject))
        .rejects.toBe()
    })
  })
})
