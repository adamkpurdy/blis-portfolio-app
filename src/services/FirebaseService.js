import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'
import 'firebase/auth'

/**
 * auth
 * https: //firebase.google.com/docs/reference/js/firebase.auth.html#callable
 * @return {Object} currentUser object from firebase
 */
export const auth = () => {
  return firebase.auth()
}

/**
 * Firestore
 * https: //firebase.google.com/docs/reference/js/firebase.firestore.Firestore.html
 *
 * @return {Interface} returns Firestore
 */
export const firestore = () => {
  return firebase.firestore()
}

/** getGeopoint GeoPoint - firebase.firestore.GeoPoint
 * https: //firebase.google.com/docs/reference/js/firebase.firestore.GeoPoint
 *
 * @param {number} lat - Latitude coordiante
 * @param {number} log - Longiture cooridante
 *
 * @return {Object} - GeoPoint https://firebase.google.com/docs/reference/js/firebase.firestore.GeoPoint#returns-geopoint
 */
export const getGeoPoint = (lat, lon) => {
  return new firebase.firestore.GeoPoint(lat, lon)
}

/** fbInit - Convienience method to initialize firebase app
 *
 * @param  {Object} config
 * @return {Object} App
 */
export const fBInit = (config) => {
  return firebase.initializeApp(config)
}

/** recaptcha RecaptchaVerifier - firebase.auth.RecaptchaVerifier
 * https: //firebase.google.com/docs/reference/js/firebase.auth.RecaptchaVerifier
 *
 * @param reCaptcha container parameter
 * @param {number} log - Longiture cooridante
 *
 * @return {Object} - GeoPoint https://firebase.google.com/docs/reference/js/firebase.firestore.GeoPoint#returns-geopoint
 */
export const recaptcha = (id) => {
  return new firebase.auth.RecaptchaVerifier(id, {
    'size': 'invisible',
    'callback': (response) => {
      // this.registerNumber()
    }
  })
}

/**
 * Storage
 * https: //firebase.google.com/docs/reference/js/firebase.storage.Storage
 *
 * @return {Interface} returns Storage
 */
export const storage = () => {
  return firebase.storage()
}

/** loginWithEmail - allows user to login via firebase
 * https: //firebase.google.com/docs/reference/js/firebase.auth.Auth.html#sign-inwith-email-and-password
 *
 * @param {String} email - A Valid email
 * @param {String} password - Password
 *
 * @return {Promist} UserCredentials
 */
export const loginWithEmail = async (email, password) => {
  const user = await firebase.auth().signInWithEmailAndPassword(email, password)
  return user
}

/** loginWithEmail - allows user to login via firebase
 * https: //firebase.google.com/docs/reference/js/firebase.auth.Auth.html#sign-inwith-phone-number
 *
 * @param {String} phoneNumber - A Valid Phonenumber. Be sure to force the user to
 * enter in the correct phonenumber format
 * @param {String} appVerifier - A verifier for domain verification and abuse prevention.
 * Currently, the only implementation is firebase.auth.RecaptchaVerifier
 *
 * @return {Promist} ConfirmationResult
 */

export const registerUserWithPhoneNumber = async (phoneNumber, appVerifier) => {
  const confirmationResult = await firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
  return confirmationResult
}

/** registerUserWithEmail - allows user to create a new user
   * https: //firebase.google.com/docs/reference/js/firebase.auth.Auth.html#create-user-with-email-and-password
   *
   * @param {String} email - A Valid email
   * @param {String} password - Password
   *
   * @return {Promise} UserCredentials
   */
export const registerUserWithEmail = async (email, password) => {
  const user = await firebase.auth().createUserWithEmailAndPassword(email, password)
  return user
}
