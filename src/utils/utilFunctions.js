/**
 * @param  {} fullName
 */
export const tokenizeFullName = (fullName) => {
  const nameAsArray = fullName.split(' ')
  const firstName = nameAsArray[0]
  const middleName = nameAsArray.slice(1, -1).join('')
  const lastName = nameAsArray[nameAsArray.length - 1]

  return { firstName, middleName, lastName }
}

export const concatFullName = (firstName, middleName, lastName) => {
  return middleName !== '' ? [firstName, middleName, lastName].join(' ') : [firstName, lastName].join(' ')
}
