// import Chatkit from '@pusher/chatkit-client'

export const sendMessage = () => {
  const { newMessage, currentUser, currentRoom } = this

  if (newMessage.trim() === '') return

  currentUser.sendMessage({
    text: newMessage,
    roomId: `${currentRoom.id}`
  })

  this.newMessage = ''
}

export const connectToRoom = (id, messageLimit = 100) => {
  this.messages = []

  const { currentUser } = this

  return currentUser
    .subscribeToRoom({
      roomId: `${id}`,
      messageLimit,
      hooks: {
        onMessage: message => {
          this.messages = [ ...this.messages, message ]
        }
      }
    })
    .then(currentRoom => {
      this.currentRoom = currentRoom
    })
}
