export default class User {
  email = ''
  firstName = ''
  isAdmin = false
  lastName = ''
  location = ''
  phoneNumber = ''
  type = 'creator'

  constructor (args) {
    Object.keys(args).forEach((v, i) => {
      this[v] = args[v]
    })

    return { ...this }
  }
}
