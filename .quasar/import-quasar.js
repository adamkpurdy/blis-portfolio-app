/**
 * THIS FILE IS GENERATED AUTOMATICALLY.
 * DO NOT EDIT.
 *
 * You are probably looking on adding startup/initialization code.
 * Use "quasar new boot <name>" and add it there.
 * One boot file per concern. Then reference the file(s) in quasar.conf.js > boot:
 * boot: ['file', ...] // do not add ".js" extension to it.
 *
 * Boot files are your "main.js"
 **/


import Vue from 'vue'

import {Quasar,QAvatar,QBadge,QBtn,QBtnDropdown,QBtnGroup,QCard,QCardSection,QCarousel,QCarouselControl,QCarouselSlide,QChip,QCardActions,QCheckbox,QDrawer,QDialog,QExpansionItem,QForm,QFooter,QHeader,QImg,QInput,QIcon,QList,QItem,QItemSection,QItemLabel,QLayout,QLinearProgress,QMenu,QOptionGroup,QPageContainer,QPage,QPagination,QRouteTab,QScrollArea,QSelect,QSeparator,QSpinnerGears,QTable,QTh,QTr,QTd,QToolbar,QTabs,QTab,QTabPanels,QTabPanel,QToolbarTitle,QVideo,Ripple,ClosePopup,Notify} from 'quasar'


Vue.use(Quasar, { config: {"notify":{"closeBtn":"dismiss","textColor":"white text-bold","timeout":0}},components: {QAvatar,QBadge,QBtn,QBtnDropdown,QBtnGroup,QCard,QCardSection,QCarousel,QCarouselControl,QCarouselSlide,QChip,QCardActions,QCheckbox,QDrawer,QDialog,QExpansionItem,QForm,QFooter,QHeader,QImg,QInput,QIcon,QList,QItem,QItemSection,QItemLabel,QLayout,QLinearProgress,QMenu,QOptionGroup,QPageContainer,QPage,QPagination,QRouteTab,QScrollArea,QSelect,QSeparator,QSpinnerGears,QTable,QTh,QTr,QTd,QToolbar,QTabs,QTab,QTabPanels,QTabPanel,QToolbarTitle,QVideo},directives: {Ripple,ClosePopup},plugins: {Notify} })
